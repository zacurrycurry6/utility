# Project Title

Zach's Utility Library

## Description

A .NET class library with useful extensions and helper services.

## Getting Started

### Dependencies

* .NET 6

## Authors

Zachary Curry
zacurrycurry@gmail.com


## Version History

* 0.1
    * Initial Release

## License

This project is licensed under the MIT License - see the LICENSE.md file for details