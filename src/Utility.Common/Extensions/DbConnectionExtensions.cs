﻿using Ardalis.GuardClauses;
using Dapper;
using Microsoft.Data.SqlClient;
using Polly;
using Polly.Retry;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace Utility.Common.Extensions
{
    public static class DbConnectionExtensions
    {
        //https://docs.microsoft.com/en-us/sql/relational-databases/errors-events/database-engine-events-and-errors?view=sql-server-ver15
        private static readonly HashSet<int> _sqlErrorNumbersToRetry = new()
        {
            -2, // timeout
            11, // general network error
            1205 // deadlock
        };

        /// <summary>
        /// Executes a command asyncronously using a task with an with an exponential backoff retry policy
        /// </summary>
        /// <typeparam name="T">The type of results to return.</typeparam>
        /// <param name="connection">The connection to query on.</param>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="parameters">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The number of seconds before command execution timeout.</param>
        /// <param name="commandType">The type of command to execute.</param>
        /// <param name="retryAttempts">Yhe maximum number of attempts to retry.</param>
        /// <returns>
        /// The number of rows affected.
        /// </returns>
        public static async Task<int> ExecuteWithRetryAsync<T>(this DbConnection connection, string sql, object? parameters = null,
            IDbTransaction? transaction = null, int? commandTimeout = null, CommandType? commandType = null, int retryAttempts = 5)
        {
            Guard.Against.Null(connection, nameof(connection));
            Guard.Against.NullOrWhiteSpace(sql, nameof(sql));

            await connection.CheckAndReOpenConnection();
            return await connection.ExecuteAsync(sql, parameters, transaction, commandTimeout, commandType)
                .WithRetry(3);
        }

        /// <summary>
        /// Executes a query asyncronously using a task with an with an exponential backoff retry policy
        /// </summary>
        /// <typeparam name="T">The type of results to return.</typeparam>
        /// <param name="connection">The connection to query on.</param>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="parameters">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The number of seconds before command execution timeout.</param>
        /// <param name="commandType">The type of command to execute.</param>
        /// <param name="retryAttempts">The maximum number of attempts to retry.</param>
        /// <returns>
        /// A sequence of data of T; if a basic type (int, string, etc) is queried then the
        /// data from the first column in assumed, otherwise an instance is created per row,
        /// and a direct column-name===member-name mapping is assumed (case insensitive).
        /// </returns>
        public static async Task<IEnumerable<T>> QueryWithRetryAsync<T>(this DbConnection connection, string sql, object? parameters = null,
            IDbTransaction? transaction = null, int? commandTimeout = null, CommandType? commandType = null, int retryAttempts = 5)
        {
            Guard.Against.Null(connection, nameof(connection));
            Guard.Against.NullOrWhiteSpace(sql, nameof(sql));

            await connection.CheckAndReOpenConnection();
            return await connection.QueryAsync<T>(sql, parameters, transaction, commandTimeout, commandType)
                .WithRetry(3);
        }

        /// <summary>
        /// Check to see see if the connection is in the 'Open' ConnectionState
        /// If the connection is broken, it will close the connection safely.
        /// Then if the ConnectionState is anything other than 'Open', it will attempt to re-open the connection
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        public static async Task CheckAndReOpenConnection(this DbConnection connection)
        {
            Guard.Against.Null(connection, nameof(connection));

            if (connection.State.HasFlag(ConnectionState.Broken))
            {
                await connection.CloseAsync();
            }

            if (!connection.State.HasFlag(ConnectionState.Open))
            {
                await connection.OpenAsync();
            }
        }

        /// <summary>
        /// https://github.com/App-vNext/Polly
        /// Retry a task a specified number of times with an exponential backoff retry policy
        /// catching general SQL exceptions such as deadlocks, timeouts, and network errors
        /// The default retry attempts of 5 will wait for
        ///  2 ^ 1 = 2 seconds then
        ///  2 ^ 2 = 4 seconds then
        ///  2 ^ 3 = 8 seconds then
        ///  2 ^ 4 = 16 seconds then
        ///  2 ^ 5 = 32 seconds
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="task">The task to retry</param>
        /// <param name="retryAttempts">The maximum number of attempts to retry</param>
        /// <returns>The value returned by the task</returns>
        public static async Task<T> WithRetry<T>(this Task<T> task, int retryAttempts = 5) =>
            await GetSqlExponentialBackoffPolicy(retryAttempts)
                .ExecuteAsync(async () => await task);

        public static AsyncRetryPolicy GetSqlExponentialBackoffPolicy(int retryAttempts = 5) =>
            Policy
                .Handle<SqlException>(ex => _sqlErrorNumbersToRetry.Contains(ex.Number))
                .WaitAndRetryAsync(
                    retryAttempts,
                    retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                    (exception, timeSpan, retryCount, context) =>
                    {
                        if (exception is SqlException sqlException)
                        {
                            Console.WriteLine($"Exception ({sqlException.Number}): {exception.Message}");
                            Console.WriteLine($"Retrying in {timeSpan.Seconds} seconds");
                        }
                    });
    }
}
