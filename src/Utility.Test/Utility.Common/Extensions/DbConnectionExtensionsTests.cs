﻿using System;
using System.Data;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using Moq;
using NUnit.Framework;
using Utility.Common.Extensions;
using Utility.Test.Helpers;

namespace Utility.Test.Utility.Common.Extensions
{
    public class DbConnectionExtensionsTests
    {
        private SqlException _sqlDeadlockException;
        private SqlException _sqlGeneralNetworkException;
        private SqlException _sqlTimeoutException;
        private SqlException _encryptionNotSupportedException;

        [SetUp]
        public void Setup()
        {
            _sqlDeadlockException = SqlExceptionHelper.Generate(1205);
            _sqlGeneralNetworkException = SqlExceptionHelper.Generate(11);
            _sqlTimeoutException = SqlExceptionHelper.Generate(-2);
            _encryptionNotSupportedException = SqlExceptionHelper.Generate(20);
        }

        [Test]
        public async Task WillRetryATaskForSqlDeadlockException()
        {
            var invocations = 0;
            var policy = DbConnectionExtensions.GetSqlExponentialBackoffPolicy(3);
            var result = await policy
                .ExecuteAsync(() => Task.FromResult(invocations++ < 1 ? throw _sqlDeadlockException : true));

            Assert.True(result);
            Assert.AreEqual(2, invocations);
        }

        [Test]
        public async Task WillRetryATaskForSqlDeadlockAndTimeoutAndNetworkException()
        {
            var invocations = 0;
            var policy = DbConnectionExtensions.GetSqlExponentialBackoffPolicy(4);
            var result = await policy
                .ExecuteAsync(() =>
                {
                    invocations++;
                    if (invocations == 1)
                    {
                        throw _sqlDeadlockException;
                    }
                    else if (invocations == 2)
                    {
                        throw _sqlTimeoutException;
                    }
                    else if (invocations == 3)
                    {
                        throw _sqlGeneralNetworkException;
                    }
                    return Task.FromResult(true);
                });

            Assert.True(result);
            Assert.AreEqual(4, invocations);
        }

        [Test]
        public async Task WillExecuteAndNotRetryATaskThatDoesNotThrowAnException()
        {
            var invocations = 0;
            var policy = DbConnectionExtensions.GetSqlExponentialBackoffPolicy(3);
            var result = await policy
                .ExecuteAsync(() => Task.FromResult(invocations++ < 1));

            Assert.True(result);
            Assert.AreEqual(1, invocations);
        }

        [Test]
        public void WillFailATaskForSqlEncryptionNotSupportedException()
        {
            var invocations = 0;
            var policy = DbConnectionExtensions.GetSqlExponentialBackoffPolicy(3);
            var ex = Assert.ThrowsAsync<SqlException>(async () => await policy.ExecuteAsync(() =>
                Task.FromResult(invocations++ <= 1 ? throw _encryptionNotSupportedException : true)));

            Assert.That(ex.Number, Is.EqualTo(20));
            Assert.AreEqual(1, invocations);
        }

        [Test]
        public async Task CanCheckAndReOpenBrokenConnection()
        {
            var connectionMock = new Mock<DbConnection>();
            connectionMock.SetupGet(x => x.State)
                .Returns(ConnectionState.Broken);

            var connection = connectionMock.Object;
            await connection.CheckAndReOpenConnection();
            connectionMock.Verify(x => x.CloseAsync(), Times.Once());
            connectionMock.Verify(x => x.OpenAsync(new CancellationToken()), Times.Once());
        }

        [Test]
        public async Task CanUseOpenConnection()
        {
            var connectionMock = new Mock<DbConnection>();
            var connection = connectionMock.Object;
            connectionMock.SetupGet(x => x.State)
                .Returns(ConnectionState.Open);

            await connection.CheckAndReOpenConnection();
            connectionMock.Verify(x => x.CloseAsync(), Times.Never());
            connectionMock.Verify(x => x.OpenAsync(new CancellationToken()), Times.Never());
        }

        [Test]
        public async Task CanOpenConnection()
        {
            var connectionMock = new Mock<DbConnection>();
            var connection = connectionMock.Object;
            await connection.CheckAndReOpenConnection();
            connectionMock.Verify(x => x.CloseAsync(), Times.Never());
            connectionMock.Verify(x => x.OpenAsync(new CancellationToken()), Times.Once());
        }

        [Test]
        public void ThrowsArgumentExceptionForNullConnection()
        {
            SqlConnection? sqlConnection = null;
            Assert.ThrowsAsync<ArgumentNullException>(async () =>
#pragma warning disable CS8604 // Possible null reference argument.
                  await DbConnectionExtensions.CheckAndReOpenConnection(sqlConnection)
#pragma warning restore CS8604 // Possible null reference argument.
            );
        }
    }
}
