﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Microsoft.Data.SqlClient;

namespace Utility.Test.Helpers
{
    internal sealed class SqlExceptionHelper
    {
        internal static SqlException Generate(int errorNumber)
        {
            var ex = (SqlException)FormatterServices.GetUninitializedObject(typeof(SqlException));
            var errors = GenerateSqlErrorCollection(errorNumber);
            SetPrivateFieldValue(ex, "_errors", errors);
            return ex;
        }

        internal static SqlErrorCollection GenerateSqlErrorCollection(int errorNumber)
        {
            var t = typeof(SqlErrorCollection);
            var col = (SqlErrorCollection)FormatterServices.GetUninitializedObject(t);
            SetPrivateFieldValue(col, "_errors", new List<object>());
            var sqlError = GenerateSqlError(errorNumber);
            var method = t.GetMethod("Add", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            method.Invoke(col, new object[] { sqlError });
            return col;
        }

        private static SqlError GenerateSqlError(int errorNumber)
        {
            var sqlError = (SqlError)FormatterServices.GetUninitializedObject(typeof(SqlError));
            SetPrivatePropertyValue(sqlError, "Number", errorNumber);
            SetPrivatePropertyValue(sqlError, "Message", string.Empty);
            SetPrivatePropertyValue(sqlError, "Procedure", string.Empty);
            SetPrivatePropertyValue(sqlError, "Server", string.Empty);
            SetPrivatePropertyValue(sqlError, "Source", string.Empty);
            return sqlError;
        }

        private static void SetPrivatePropertyValue(object obj, string property, object val)
        {
            var member = obj.GetType().GetProperty(property);
            member.SetValue(obj, val);
        }

        private static void SetPrivateFieldValue(object obj, string field, object val)
        {
            var member = obj.GetType().GetField(field, System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            member.SetValue(obj, val);
        }
    }
}
